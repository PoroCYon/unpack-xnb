
default: debug

%/:
	@mkdir -p "$@"

CC_ ?= clang
CC:=$(CC_)

LIBS=
# you can add -DSTB_IMAGE_WRITE_STATIC if you want stb_image_write to
# be linked statically
CFLAGS:=-Iinc -Istb -Iwimlib/include -std=gnu11 $(CFLAGS)
LNFLAGS=

ifneq (,$(findstring clang,$(CC)))
	CFLAGS+=-Weverything -Wpedantic -Wno-language-extension-token \
            -Wno-flexible-array-extensions -Wno-vla -Wno-switch \
            -Wno-gnu-statement-expression -Wno-switch-default \
            -Wno-gnu-zero-variadic-macro-arguments
else
	CFLAGS+=-Wall -Wno-unknown-pragmas -Wno-switch
endif

obj/%.o: src/%.c
	$(CC) $(CFLAGS) -o "$@" -c "$<" >/dev/null #2>&1

SRCFILES := $(wildcard src/*.c)
OBJFILES := $(patsubst src/%.c,obj/%.o,$(SRCFILES))

bin/unpack-xnb: $(OBJFILES)
	$(CC) $(CFLAGS) -o "$@" $^ $(LIBS)

.clang_complete:
	@echo "$(CFLAGS)" | sed 's/  */\n/g' > .clang_complete
	@< .clang_complete sed 's/\-I/\-I\.\.\//g' > src/.clang_complete

all: obj/ bin/ bin/unpack-xnb

debug: CFLAGS += -DDEBUG=printf \
    -DENABLE_LZX_DEBUG -DENABLE_ASSERTIONS \
    -g -Og -fsanitize=undefined #-fsanitize=address
debug: LNFLAGS += -fsanitize=undefined #-fsanitize=address
debug: all

release: CFLAGS += -DNODEBUG -DRELEASE -O3
release: all

clean:
	@-rm -rvf obj/
	@-rm -rvf bin/

.PHONY: default all debug release clean

