# unpack-xnb

An unpacker for `XNB` files. Currently, the following `XNB` types are
supported:

* `SoundEffect` (to a PCM WAV file)
* `WaveBank` (an `XWB` file, to a bunch of PCM/ADPCM WAV files. It does
  currently not parse cue names.)
* `Effect` (to a compiled DX shader file, to be analyzed with your
  favourite tool)
* `Texture2D` (to a PNG file)

Support for the following types are currently being worked on:

* `SpriteFont` (to a PNG file, and a file describing glyph properties)
* `DynamicSpriteFont` (to PNG files, and a file describing glyph
  properties) (NOTE: this type is specific to ReLogic games.)

## TODO:

* `SpriteFont` parsing
* `DynamicSpriteFont` parsing
* xact globals, sound bank data?

## Greets

* **flibit** for FNA, where I based most of my code on.
* **aluigi** for unxwb, which I read to understand the ADPCM WAV, WMA
  and XMA formats.
* **gameking008** for writing a very simple `SoundEffect` unpacker,
  which made me understand the basic structure of `XNB` files.
  (The unpacker itself is slightly off, though `:P`.)

