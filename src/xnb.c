
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "wimlib/decompressor_ops.h"

#include "util.h"

#include "xnb.h"

#include "texture2d.h"
#include "spritefont.h"
#include "soundeffect.h"
#include "effect.h"
#include "wavebank.h"

#pragma pack(push,1)
struct uxnb_xnb_fmt_ {
    char magic[3];
    uint8_t platform;
    uint8_t xnaver;
    uint8_t flags;
    uint32_t filesz;
};
#pragma pack(pop)

struct uxnb_xnb_file_ {
    struct uxnb_xnb_fmt* compr;

    uint16_t platform;
    uint16_t xnaver;
    enum uxnb_flags flags;

    struct uxnb_object obj;

    size_t nsharedresources;
};

extern bool quiet_flag; // hack

void eprintfn(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    vfprintf(stderr, fmt, args);
#pragma clang diagnostic pop
    va_end(args);
    fprintf(stderr, "%c", '\n');
}

void uxnb_free_dtorable(struct uxnb_dtorable* o) {
    if (o) {
        if (o->free_me) {
            o->free_me(o);
        }

        free(o);
    }
}

static bool validate(struct uxnb_xnb_fmt* f) {
    return f->magic[0] == UXNB_XNB_MAGIC[0] && f->magic[1] == UXNB_XNB_MAGIC[1]
                                            && f->magic[2] == UXNB_XNB_MAGIC[2];
}
static bool supported_ver(struct uxnb_xnb_fmt* f) {
    return f->xnaver >= 4 && f->xnaver <= 5;
}

static void* uxnb_read_object(const char* rtype, void* data, size_t* sz, struct uxnb_xnb_fmt* fmt) {
    *sz = ~(size_t)0;

    size_t l = strlen(rtype);
    for (size_t i = 0; i < l; ++i) {
        char c = rtype[i];

        if ((c <  'A' || c >  'Z') &&
            (c <  'a' || c >  'z') &&
            (c <  '0' || c >  '9') &&
             c != '.' && c != '_'    ) {
            l = i;

            if (!quiet_flag) {
                char sfmt[0x40];
                snprintf(sfmt, 0x3F, "Note: shortening '%%s' to '%%.%zus'.\n", l);
                sfmt[0x3F] = '\0';
                eprintf_backend(sfmt, rtype, rtype);
            }
            break;
        }
    }

    uint32_t h = strn_hash_djb2(rtype, l);
    switch (h) {
    case 0x00DF663D: // MS Texture2DReader
    case 0xA9A7F905: // MG Texture2DReader
        return uxnb_read_texture2d(data, sz, fmt, NULL);
    case 0x65C67784: // MS SpriteFontReader
    case 0x27A1634C: // MG SpriteFontReader
        return uxnb_read_spritefont(data, sz, fmt);
    case 0xDBC49E6C: // Microsoft.Xna.Framework.Content.SoundEffectReader
    case 0xD8FD0334: // MonoGame.Framework.Content.SoundEffectReader
        return uxnb_read_soundeffect(data, sz, fmt);
    case 0xC0E116E3: // MS EffectReader
    case 0xD9CD5EAB: // MG EffectReader
        return uxnb_read_effect(data, sz, fmt);
    case 0xBF38C409: // Microsoft.Xna.Framework.Audio.WaveBank
    case 0x5B9CD5D1: // MonoGame.Framework.Audio.WaveBank
        return uxnb_read_wavebank(data, sz, fmt);
  //case 0x41B03B7D: // ReLogic.Graphics.DynamicSpriteFontReader
  //    break;
    }

    eprintf("Unknown XNB type '%s', hash result '%X'\n", rtype, h);
    return NULL;
}

static struct uxnb_xnb_fmt* xnb_decompress(void* base,
        struct uxnb_xnb_fmt* fmt, size_t filesz, size_t* nfsz) {
    bool realign = (size_t)base & 0x3;
    if (realign) {
        size_t s = filesz - ((size_t)base - (size_t)fmt);
        void* b_ = malloc(filesz);
        memcpy(b_, base, s);
        base = b_;
    }

    size_t dsize = *(uint32_t*)base;
    uint8_t* d = (uint8_t*)base + sizeof(uint32_t);
    size_t csize = filesz - sizeof(struct uxnb_xnb_fmt_) - sizeof(uint32_t); // minus dsize

    size_t nsz = dsize + sizeof(struct uxnb_xnb_fmt_);
    if (nfsz) {
        *nfsz = nsz;
    }

    eprintf("decompress %zu bytes->%zu bytes\n", csize, dsize);

    struct uxnb_xnb_fmt* ret = (struct uxnb_xnb_fmt*)calloc(1, nsz);
    memcpy(ret->magic, fmt->magic, sizeof(fmt->magic));
    ret->xnaver = fmt->xnaver;
    ret->flags  = fmt->flags;// & ~uxnbf_compressed;
    ret->filesz = (uint32_t)nsz;

    FILE* filed = fopen("lzxdump.lzx", "wb");
    fwrite(d, 1, csize, filed);
    fclose(filed);

    int stat;
    void* priv;
    stat = lzx_decompressor_ops.create_decompressor(0x10000, &priv);
    eprintf("priv=%p, stat=%d\n", priv, stat);
    if (stat) {
        if (realign) free(base);
        return NULL;
    }

    uint8_t* dest = &ret->_data[0];
    for (uint8_t* dend = d + csize; d < dend; ) {
        uint32_t hi = *d; ++d;
        uint32_t lo = *d; ++d;

        uint32_t block_size = (hi << 8) | lo;
        uint32_t frame_size = 0x8000;

        if (hi == 0xFF) {
            hi = lo;
            lo = *d; ++d;
            frame_size = (hi << 8) | lo;
            hi = *d; ++d;
            lo = *d; ++d;
            block_size = (hi << 8) | lo;
        }

        eprintf("bs=%lu, fs=%lu\n", block_size, frame_size);

        if (block_size == 0 || frame_size == 0) {
            break; // the end!
        }

        stat = lzx_decompressor_ops.decompress(d, block_size,
                dest, frame_size, priv);
        d += block_size;
        dest += frame_size;

        eprintf("stat=%d\n", stat);
        if (stat) {
            lzx_decompressor_ops.free_decompressor(priv);
            if (realign) free(base);
            return NULL;
        }
    }
    if (dest != &ret->_data[0] + dsize) {
        eprintf("decompr size mismatch! expected %zu, got %zu\n",
                dsize, dest-&ret->_data[0]);
        return NULL;
    }

    lzx_decompressor_ops.free_decompressor(priv);

    filed = fopen("lzxdump.xnb", "wb");
    fwrite(ret, 1, sizeof(struct uxnb_xnb_fmt_) + dsize, filed);
    fclose(filed);

    if (realign) {
        free(base);
    }
    return ret;
}

static void* reado_ensure_aligned(const char* rtype, void* data, size_t* sz,
        struct uxnb_xnb_fmt* fmt, size_t filesz,
        size_t ntypes, char** types) {
    static const size_t ALIGN = 4;

    if ((size_t)data & (ALIGN - 1)) {
        // oh shi-!
        size_t s = filesz - ((size_t)data - (size_t)fmt);
        void* nd = malloc(s);
        memcpy(nd, data, s);
        void* r = uxnb_read_object(rtype, nd, sz, fmt);
        free(nd);
        return r;
    }

    return uxnb_read_object(rtype, data, sz, fmt);
}
struct uxnb_xnb_file* uxnb_read_xnb(void* data, const char* typehint, size_t filesz) {
    if (!data) {
        return NULL;
    }

    struct uxnb_xnb_fmt* fmt = (struct uxnb_xnb_fmt*)data, *of = fmt;

    if (!validate(fmt)) {
        eprintf("Invalid XNB header.\n");
        return NULL;
    }
    if (!supported_ver(fmt)) {
        eprintf("XNB version %d not supported!\n", fmt->xnaver);
        return NULL;
    }

    size_t fdatasz = filesz;
    if (fmt->flags & uxnbf_compressed_lzx) {
        fmt = xnb_decompress(&fmt->_data[0], fmt, filesz, &fdatasz);
        if (!fmt) {
            eprintf("XNB decompression failed.\n");
            return NULL;
        }

        if (!validate(fmt)) {
            eprintf("Invalid XNB header in compressed data.\n");
            return NULL;
        }
        if (!supported_ver(fmt)) {
            eprintf("XNB version %d of compressed data not supported!\n",
                    fmt->xnaver);
            return NULL;
        }
    } else if (fmt->flags & uxnbf_compressed_lz4) {
        eprintf("LZ4 decompression not supported yet.\n");
        return NULL;
    }

    uint8_t* d = &fmt->_data[0];
    size_t nreaders = readvlq(&d);

    if (nreaders == 0) {
        if (fmt->flags & (uxnbf_compressed_lzx|uxnbf_compressed_lz4)) {
            free(fmt);
        }

        eprintf("No type readers.\n");
        return NULL;
    }

    char* readers[nreaders];

    for (size_t i = 0; i < nreaders; ++i, d += sizeof(uint32_t)) {
        readers[i] = mksz(readlps(&d));
        eprintf("readers[%d] = '%s'\n", i, readers[i]);
        // version == *(uint32_t*)d, but who cares
    }

    size_t nshres = readvlq(&d);
    struct uxnb_object shres[nshres];

    size_t typeind = readvlq(&d);
    if (!typeind && !quiet_flag) {
        eprintf("Invalid type ID, data has no type. Assuming main type...\n");
        typeind = 1;
    }

    struct uxnb_object maino;

    maino.rawdata = d;
    if (!(maino.rtype = readers[typeind-1])) {
        maino.rtype = typehint;
    }

    maino.data = reado_ensure_aligned(maino.rtype, d, &maino.size,
            fmt, fdatasz, nreaders, readers);

    for (size_t i = 0; i < nshres; ++i) {
        size_t readerind = readvlq(&d);
        const char* rt = readers[readerind];

        shres[i].rtype   = rt;
        shres[i].rawdata = d ;
        if (!(shres[i].data = reado_ensure_aligned(rt, d, &shres[i].size,
                        fmt, fdatasz, nreaders, readers))) {
            eprintf("Warning: couldn't read shared resource %zu of "
                    "type '%s'.\n", i, rt);
        }
    }

    struct uxnb_xnb_file* r = (struct uxnb_xnb_file*)
        calloc(1, sizeof(struct uxnb_xnb_file_));

    r->platform = fmt->platform;
    r->xnaver   = fmt->xnaver;
    r->flags    = (enum uxnb_flags)fmt->flags;

    r->obj = maino;

    r->nsharedresources = nshres;
    memcpy(r->sharedresources, shres, sizeof(struct uxnb_object) * nshres);

    if (of->flags & (uxnbf_compressed_lzx|uxnbf_compressed_lz4)) {
        r->compr = fmt;
    }
    else {
        r->compr = NULL;
    }

    return r;
}

inline static void free_obj(struct uxnb_object o) {
    uxnb_free_dtorable(o.data);

    if (o.rtype) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-qual"
        free((void*)o.rtype);
#pragma clang diagnostic pop
    }
}

void uxnb_free_file(struct uxnb_xnb_file* file) {
    if (!file) {
        return;
    }

    for (size_t i = 0; i < file->nsharedresources; ++i) {
        free_obj(file->sharedresources[i]);
    }

    free_obj(file->obj);

    if (file->compr) {
        free(file->compr);
    }

    free(file);
}

