
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef STB_IMAGE_WRITE_STATIC
#define STB_IMAGE_WRITE_IMPLEMENTATION
#endif
#define STBI_WRITE_NO_STDIO
#include "stb_image_write.h"

#include "util.h"
#include "xnb.h"
#include "dxt.h"

#include "texture2d.h"

enum surface_fmt {
    // TODO: support others?
    sf_argb =  0,
    sf_ext1 =  2, // FIXME: unsupported!
    sf_dxt1 =  4,
    sf_dxt3 =  5,
    sf_dxt5 =  6,
    sf_bgra = 20,
};

struct tex2d_fmt {
    uint32_t format;
    uint32_t width ;
    uint32_t height;
    uint32_t mmLevels;
    uint32_t data[]; // {bloblen; uint8_t blob[bloblen]}[mmLevels]
};

struct resizearr {
    void* blob;
    size_t cap;
    size_t sz;
};

static void tex2d_free(struct uxnb_dtorable* o) {
    if (!o || o->free_me != tex2d_free) {
        return;
    }

    struct uxnb_texture2d* t = (struct uxnb_texture2d*)o;

    for (size_t i = 0; i < t->nlevels; ++i) {
        if (t->levels[i].png) {
            free(t->levels[i].png);
        }
    }
}
static bool tex2d_decode(enum surface_fmt fmt, uint32_t* dest, void* src,
        size_t w, size_t h, size_t datasz) {
    switch (fmt) {
        case sf_argb:
            memcpy(dest, src, datasz);
            return true;
        case sf_bgra:
            for (size_t i = 0; i < (datasz >> 2); ++i) {
                uint32_t v = ((uint32_t*)src)[i];
                dest[i] = (v & 0xFF000000)
                        | (__builtin_bswap32(v) >> 8);
            }
            return true;
        case sf_dxt1:
            uxnb_unpack_dxt1(dest, src, w, h);
            return true;
        case sf_dxt3:
            uxnb_unpack_dxt3(dest, src, w, h);
            return true;
        case sf_dxt5:
            uxnb_unpack_dxt5(dest, src, w, h);
            return true;
    }

    eprintf("Error: unknown surface format %i\n", fmt);
    return false;
}
static void tex2d_write_fn(void* ctx, void* data, int sz) {
    struct resizearr* a = (struct resizearr*)ctx;

    if (a->sz + (size_t)sz > a->cap) {
        do {
            a->cap <<= 1;
        } while (a->sz + (size_t)sz > a->cap);

        a->blob = realloc(a->blob, a->cap);
    }

    memcpy((uint8_t*)a->blob + a->sz, data, (size_t)sz);
    a->sz += (size_t)sz;
}
struct uxnb_texture2d* uxnb_read_texture2d(void* data, size_t* sz,
        struct uxnb_xnb_fmt* fmt, size_t* bsz) {
    size_t bs = sizeof(struct tex2d_fmt);
    struct tex2d_fmt* tf = (struct tex2d_fmt*)data;

    enum surface_fmt sf;

    if (fmt->xnaver < 5) {
        switch (tf->format) {
            case 1:
                sf = sf_bgra;
                break;
            case 28:
                sf = sf_dxt1;
                break;
            case 30:
                sf = sf_dxt3;
                break;
            case 32:
                sf = sf_dxt5;
                break;
            default:
                eprintf("Unsupported legacy surface format %u\n", tf->format);
                return NULL;
        }
    } else {
        sf = (enum surface_fmt)tf->format;
    }

    size_t allocsz = sizeof(uxnb_dtor_f) + sizeof(size_t) +
            sizeof(struct uxnb_tex2d_level) * tf->mmLevels;
    if (sz) {
        *sz = allocsz;
    }

    struct uxnb_texture2d* r =
        (struct uxnb_texture2d*)calloc(1, allocsz);
    r->free_me = tex2d_free;
    r->nlevels = tf->mmLevels;

    uint32_t* cd = &tf->data[0];
    for (size_t i = 0; i < tf->mmLevels; ++i) {
        uint32_t datasz = *cd; ++cd;
        bs += sizeof(uint32_t);

        uint32_t w = tf->width  >> i,
                 h = tf->height >> i;

        // not allocating this on the stack, as it might get huge
        uint32_t* rdata = (uint32_t*)calloc(sizeof(uint32_t), (size_t)w * (size_t)h);
        if (!tex2d_decode(sf, rdata, cd, w, h, datasz)) {
            free(rdata);
            uxnb_free_dtorable((struct uxnb_dtorable*)r);
            return NULL;
        }
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
        cd = (uint32_t*)((uint8_t*)cd + datasz);
#pragma clang diagnostic pop
        bs += datasz;

        r->levels[i].width  = w;
        r->levels[i].height = h;

        struct resizearr aa;
        aa.blob = malloc(aa.cap = 0x10000);
        aa.sz ^= aa.sz;
        stbi_write_png_to_func(tex2d_write_fn, &aa, (int)w, (int)h, 4, rdata, (int)(w << 2));
        free(rdata);

        r->levels[i].filesz = aa.sz;
        r->levels[i].png    = realloc(aa.blob, aa.sz);
    }

    /*if (sz) {
        *sz = r->levels[0].filesz;
    }*/
    if (bsz) {
        *bsz = bs;
    }
    return r;
}

