
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "util.h"
#include "wave.h"
#include "xnb.h"
#include "soundeffect.h"

#include "wavebank.h"

enum wavebank_flags {
    wbf_has_names = 0x0001,
    wbf_compact   = 0x0002,
    wbf_no_sync   = 0x0004,
    wbf_seektbl   = 0x0008
};

#pragma pack(push,1)
struct offlen {
    uint32_t off;
    uint32_t len;
};
struct wavebank_header {
    uint32_t magic;
    uint32_t aever;
    uint32_t wbver;
    struct offlen ols[5];
};
struct wavebank_h2 {
    uint16_t streaming;
    uint16_t flags;
    uint32_t nentries;
    char wbname[0x40];
    uint32_t metasz;
    uint32_t namesz;
    uint32_t align;
};
#pragma pack(pop)

static void wavebank_free(struct uxnb_dtorable* d) {
    struct uxnb_wavebank* wb = (struct uxnb_wavebank*)d;

    if (!wb) {
        return;
    }
    if (wb->free_me != wavebank_free /* wtf? */) {
        wb->free_me(d);
        return;
    }

    for (size_t i = 0; i < wb->nentries; ++i) {
        void* a = wb->entries[i].audiofile.pcm;
        if (a) {
            free(a);
        }
    }

    wb->free_me = NULL;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
struct uxnb_wavebank* uxnb_read_wavebank(void* data, size_t* sz, struct uxnb_xnb_fmt* _) {
#pragma clang diagnostic pop
    struct wavebank_header* wb = (struct wavebank_header*)data;

    if (wb->magic != to_magic(UXNB_WB_MAGIC)) {
        eprintf("Invalid XWB header.\n");
        return NULL;
    }

    if (wb->aever != UXNB_WB_AEVERSION || wb->wbver != UXNB_WB_WBVERSION) {
        eprintf("Unsupported file version.\n");
        return NULL;
    }

    struct wavebank_h2* h2 = (struct wavebank_h2*)((uint8_t*)wb + wb->ols[0].off);

    uint32_t playROff = wb->ols[4].off;
    if (!playROff) {
        playROff = wb->ols[1].off + (h2->nentries * h2->metasz);
    }

    struct uxnb_waveentry_meta fmt;
    *(uint32_t*)&fmt = 0;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
    uint32_t* h2e = (uint32_t*)(h2 + 1);
#pragma clang diagnostic pop

    if (h2->flags & wbf_compact) {
        fmt = *(struct uxnb_waveentry_meta*)h2e;
    }

    uint32_t coff = wb->ols[1].off;

    struct uxnb_wavebank* r = (struct uxnb_wavebank*)
        calloc(1, sizeof(uxnb_dtor_f) + sizeof(size_t)
                + sizeof(struct uxnb_waveentry) * h2->nentries);
    r->nentries = h2->nentries;
    r->free_me  = wavebank_free;

    for (uint32_t ce = 0; ce < h2->nentries; ++ce) {
        uint32_t playOff, playLen, loopOff, loopLen;
        playOff = playLen = loopOff = loopLen = 0;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
        uint32_t* cd = (uint32_t*)((uint8_t*)wb + coff);
#pragma clang diagnostic pop

        if (h2->flags & wbf_compact) {
            uint32_t el = *cd;

            playOff = (el & ((1 << 21) - 1)) * h2->align;
            playLen = (el >> 21) & ((1 << 11) - 1);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
            cd = (uint32_t*)((uint8_t*)cd + h2->metasz);
#pragma clang diagnostic pop

            if (ce == h2->nentries - 1) {
                el = wb->ols[4].len;
            }
            else {
                el = (*cd & ((1 << 21) - 1)) * h2->align;
            }

            playLen = el - playOff;
        }
        else {
            if (h2->metasz >= 0x04) {
                ++cd; // flags, duration
            }
            if (h2->metasz >= 0x08) {
                fmt = *(struct uxnb_waveentry_meta*)cd; ++cd;
            }
            if (h2->metasz >= 0x0C) {
                playOff = *cd; ++cd;
            }
            if (h2->metasz >= 0x10) {
                playLen = *cd; ++cd;
            }
            if (h2->metasz >= 0x14) {
                loopOff = *cd; ++cd;
            }
            if (h2->metasz >= 0x18) {
                loopLen = *cd; ++cd;
            }
            else if (playLen) {
                playLen = wb->ols[4].len;
            }
        }

        coff    += h2->metasz;
        playOff += playROff  ;

        uint8_t* wdata = (uint8_t*)wb + playOff;

        switch (fmt.codec) {
            case uxnbwbc_pcm:
                {
                    size_t wavesz = sizeof(struct uxnb_wavefmt_pcm) + playLen;
                    struct uxnb_wave_pcm* waved = (struct uxnb_wave_pcm*)malloc(wavesz);
                    uxnb_wave_fill_pcm_header(&waved->fmt, wavesz);
                    if (sz) {
                        *sz = wavesz;
                    }

                    waved->fmt.shared.channels   = fmt.channels;
                    waved->fmt.shared.samplerate = fmt.samplerate;
                    waved->fmt.shared.byterate   = playLen / fmt.samplerate;
                    waved->fmt.shared.blockalign = fmt.alignment;
                    waved->fmt.shared.bitdepth   = fmt.bitdepth ? 0x10 : 0x08;

                    memcpy(&waved->samples[0], wdata, playLen);

                    r->entries[ce].filesz        = wavesz;
                    r->entries[ce].meta          = fmt   ;
                    r->entries[ce].audiofile.pcm = waved ;
                }
                break;
            case uxnbwbc_xma:
                // TODO: add XMA support
                eprintf("Warning: cue %zu is stored in the XMA format, which is unsupported.\n", ce);
                break;
            case uxnbwbc_adpcm:
                {
                    size_t wavesz = sizeof(struct uxnb_wavefmt_adpcm) + playLen;
                    struct uxnb_wave_adpcm* waved = (struct uxnb_wave_adpcm*)malloc(wavesz);
                    uxnb_wave_fill_adpcm_header(&waved->fmt, wavesz);
                    if (sz) {
                        *sz = wavesz;
                    }

                    waved->fmt.shared.channels   = fmt.channels;
                    waved->fmt.shared.samplerate = fmt.samplerate;

                    waved->fmt.shared.blockalign = (fmt.alignment + 22) * fmt.channels;
                    waved->fmt.cb.samples_block
                        = (uint16_t)((  ((waved->fmt.shared.blockalign - (7 * fmt.channels)) << 3)
                           / (UXNB_ADPCM_BITDEPTH * fmt.channels)
                          ) + 2);
                    waved->fmt.shared.byterate = 21 * waved->fmt.shared.blockalign;

                    if (fmt.alignment && fmt.channels) {
                        uint32_t w = (uint32_t)( ((fmt.alignment - (7 * fmt.channels)) << 3)
                            / waved->fmt.shared.bitdepth);
                        w = (playLen / fmt.alignment) * w;
                        waved->fmt.fact_data = w / fmt.channels;
                    }
                    else {
                        waved->fmt.fact_data = 0;
                    }

                    memcpy(&waved->samples[0], wdata, playLen);

                    r->entries[ce].filesz          = wavesz;
                    r->entries[ce].meta            = fmt   ;
                    r->entries[ce].audiofile.adpcm = waved ;
                }
                break;
            case uxnbwbc_wma:
                //eprintf("Warning: cue %zu is stored in the WMA format, which is unsupported.\n", ce);
                {
                    uint32_t bps = (fmt.alignment > UXNB_XWMA_AVGBPSES)
                        ? uxnb_wma_avgbps[fmt.alignment >> 5]
                        : uxnb_wma_avgbps[fmt.alignment     ];
                    uint16_t balign = uxnb_wma_blockalign[fmt.alignment & 0xF];

                    uint32_t pktlen = balign,
                             pktnum = playLen / pktlen,
                             pktsz  = pktnum * sizeof(uint32_t);

                    size_t xwmasz = sizeof(struct uxnb_wavefmt_xwma)+pktsz
                        + sizeof(struct uxnb_wavefmt_xwma_) + playLen;

                    struct uxnb_wave_xwma* xwmad = (struct uxnb_wave_xwma*)calloc(1, xwmasz);
                    struct uxnb_wave_xwma_* xdata = (struct uxnb_wave_xwma_*)
                        ((size_t)&xwmad->dpds[0] + pktsz);
                    uxnb_wave_fill_xwma_header(&xwmad->fmt, pktnum );
                    uxnb_wave_fill_xwma_data  (&xdata->hdr, playLen);
                    if (sz) {
                        *sz = xwmasz;
                    }

                    xwmad->fmt.header.riff.size  = xwmasz - sizeof(struct uxnb_riff);
                    xwmad->fmt.shared.channels   = fmt.channels;
                    xwmad->fmt.shared.samplerate = fmt.samplerate;
                    xwmad->fmt.shared.byterate   = bps;
                    xwmad->fmt.shared.blockalign = balign;

                    uint32_t fullsz = ((playLen * bps) & 0xfff)
                            ? (uint32_t)((1 + (((uint64_t)playLen * bps) >> 12)) << 12)
                            : playLen,
                        allblks   = fullsz >> 12,
                        avgbppkt  = allblks / pktnum,
                        spareblks = allblks - avgbppkt * pktnum;

                    uint32_t acc = 0;
                    for (size_t i = 0; i < pktnum; ++i) {
                        acc += avgbppkt << 12;
                        if (spareblks != 0) {
                            acc += 0x1000;
                            --spareblks;
                        }
                        xwmad->dpds[i] = acc;
                    }

                    memcpy(&xdata->data[0], wdata, playLen);

                    r->entries[ce].filesz = xwmasz;
                    r->entries[ce].meta   = fmt   ;

                    r->entries[ce].audiofile.xwma = xwmad;
                }
                break;
            default:
                eprintf("???\n");
                break;
        }
    }

    return r;
}

