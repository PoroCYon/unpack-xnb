
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "xnb.h"
#include "texture2d.h"

#include "spritefont.h"

static void sprf_dtor(struct uxnb_dtorable* o) {
    struct uxnb_spritefont* sf = (struct uxnb_spritefont*)o;

    if (sf->free_me != sprf_dtor) {
        return;
    }

    if (sf->texture) {
        uxnb_free_dtorable((struct uxnb_dtorable*)sf->texture);
    }

    if (sf->glyphs) {
        free(sf->glyphs);
    }
    if (sf->crops) {
        free(sf->crops);
    }
    if (sf->charmap) {
        free(sf->charmap);
    }
    if (sf->kerning) {
        free(sf->kerning);
    }

    o->free_me = NULL;
}

struct uxnb_spritefont* uxnb_read_spritefont(void* data, size_t* sz,
        struct uxnb_xnb_fmt* fmt) {
    // TODO: align
    size_t tsz;
    struct uxnb_texture2d* t2d =
        uxnb_read_texture2d((void*)((uint8_t*)data + 1 /* if the code
            breaks, try to remove this */), NULL, fmt, &tsz);

    struct uxnb_spritefont* r = (struct uxnb_spritefont*)
        calloc(1, sizeof(struct uxnb_spritefont));

    r->free_me = sprf_dtor;
    r->texture = t2d;

    /*  const texture = resolver.read(buffer);
     *  const glyphs = resolver.read(buffer);
     *  const cropping = resolver.read(buffer);
     *  const characterMap = resolver.read(buffer);
     *  const verticalLineSpacing = int32Reader.read(buffer);
     *  const horizontalSpacing = singleReader.read(buffer);
     *  const kerning = resolver.read(buffer);
     *  const defaultCharacter = nullableCharReader.read(buffer);
     */

    return r;

    // don't bother reading the rest, as we're not writing it out
    // (the current code's borked anyway - somehow, there are random
    // zero bytes right before the lengths, and the lengs themselves
    // look off as well...)

    /*uint8_t* d = (uint8_t*)data + tsz + 1;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
    uint32_t* du = (uint32_t*)d;
#pragma clang diagnostic pop

    // read glyphs
    uint32_t nglyphs = *du; ++du;
    struct uxnb_rect glyphs[nglyphs];

    struct uxnb_rect* dr = (struct uxnb_rect*)du;
    for (size_t i = 0; i < nglyphs; ++i, ++dr) {
        glyphs[i] = *dr;
    }

    // read croppings
    du = (uint32_t*)dr;
    uint32_t ncrops = *du; ++du;
    struct uxnb_rect crops[nglyphs];

    dr = (struct uxnb_rect*)du;
    for (size_t i = 0; i < nglyphs; ++i, ++dr) {
        crops[i] = *dr;
    }

    // read charmap
    du = (uint32_t*)dr;
    uint32_t nchars = *du; ++du;
    char charmap[nchars];

    d = (uint8_t*)du;
    for (size_t i = 0; i < nchars; ++i, ++d) {
        charmap[i] = (char)*d;
    }

    // read line spacing
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
    du = (uint32_t*)d;
#pragma clang diagnostic pop
    int32_t line_spacing = *(int32_t*)du; ++du;

    // read spacing
    float* df = (float*)du;
    float spacing = *df; ++df;

    // read kerning
    du = (uint32_t*)df;
    uint32_t nkerns = *du; ++du;
    eprintf("nkerns=%u\n", nkerns);
    struct uxnb_vec3 kerns[nkerns];

    struct uxnb_vec3* dv = (struct uxnb_vec3*)du;
    for (size_t i = 0; i < nkerns; ++i, ++dv) {
        kerns[i] = *dv;
    }

    // read default char
    d = (uint8_t*)dv;
    bool has_default = *d; ++d;
    char default_char = -1; // ~ auto-casts it to an int >__>

    if (has_default) {
        default_char = (char)*d; ++d;
    }

    size_t sss = (size_t)data - (size_t)d;
    if (sz) {
        *sz = sss;
    }

    // ----------------------

    struct uxnb_spritefont* r = (struct uxnb_spritefont*)
        calloc(1, sizeof(struct uxnb_spritefont));

    r->free_me = sprf_dtor;
    r->texture = t2d;

    r->nglyphs = nglyphs;
    r->ncrops  = ncrops ;
    r->nchars  = nchars ;
    r->nkerns  = nkerns ;

    r->linespacing  = line_spacing;
    r->spacing      = spacing;
    r->default_char = default_char;

    r->glyphs  = (struct uxnb_rect*)calloc(nglyphs, sizeof(struct uxnb_rect));
    r->crops   = (struct uxnb_rect*)calloc(nglyphs, sizeof(struct uxnb_rect));
    r->charmap = (char            *)calloc(nglyphs, sizeof(char            ));
    r->kerning = (struct uxnb_vec3*)calloc(nglyphs, sizeof(struct uxnb_vec3));

    memcpy(r->glyphs , glyphs , nglyphs * sizeof(struct uxnb_rect));
    memcpy(r->crops  , crops  , ncrops  * sizeof(struct uxnb_rect));
    memcpy(r->kerning, kerns  , nkerns  * sizeof(struct uxnb_vec3));
    memcpy(r->charmap, charmap, nchars  * sizeof(char            ));

    return r;*/
}

