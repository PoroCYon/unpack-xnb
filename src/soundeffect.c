
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "wave.h"
#include "xnb.h"

#include "soundeffect.h"

#pragma pack(push, 1)
struct xnbfmt {
    uint32_t fmt_size;
    struct uxnb_wave_shared _;
    uint16_t cbsize;
};
struct xnbfmt_data {
    uint32_t datasz;
    uint8_t samples[];
};
struct xnbfmt_end {
    uint32_t loopstart;
    uint32_t looplen;
    uint32_t songlen;
};
#pragma pack(pop)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
struct uxnb_soundeffect* uxnb_read_soundeffect(void* data, size_t* sz, struct uxnb_xnb_fmt* _) {
#pragma clang diagnostic pop
    struct xnbfmt* fmt = (struct xnbfmt*)data;

    struct xnbfmt_data* d = (struct xnbfmt_data*)(
            (uint8_t*)&fmt->cbsize + sizeof(uint16_t)
             + fmt->fmt_size - (sizeof(struct uxnb_wave_shared)
                                + sizeof(uint16_t)/* cbsize */));
    struct xnbfmt_end* e = (struct xnbfmt_end *)(&d->samples[0] + d->datasz);

    size_t padsz = d->datasz;
    if (padsz & 1) { // alignment mandated by WAV standard
        ++padsz;
    }
    size_t wavesz = sizeof(struct uxnb_wavefmt_pcm) + padsz;
    if (sz) {
        *sz = wavesz;
    }

    struct uxnb_soundeffect* r = (struct uxnb_soundeffect*)
        malloc(sizeof(size_t) + sizeof(uint32_t) * 3
                              + wavesz + sizeof(uxnb_dtor_f));

    r->free_me   = NULL;
    r->wavesz    = wavesz;
    r->loopstart = e->loopstart; r->looplength = e->looplen; r->length_ms = e->songlen;

    uxnb_wave_fill_pcm_header(&r->wavfile.fmt, wavesz);

    if (fmt->_.format != uxnbwf_pcm) {
        eprintf("Warning: Wave format isn't PCM. Continuing...\n");
    }
    memcpy(&r->wavfile.fmt.shared, &fmt->_, sizeof(struct uxnb_wave_shared));

    memcpy(&r->wavfile.samples[0], &d->samples[0], d->datasz);

    return r;
}

