
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#ifndef _WIN32
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>
#endif

#include "util.h"
#include "xnb.h"
#include "texture2d.h"
#include "spritefont.h"
#include "soundeffect.h"
#include "wavebank.h"
#include "effect.h"

#define COPYRIGHT_MSG "Copyright (C) 2017 PoroCYon, GPLv>=3\n"

enum xnb_type {
    xnbt_tex, xnbt_fnt, xnbt_snd, xnbt_wb, xnbt_sh//, xnbt_dsf
};
#ifndef _WIN32
struct mmap_args { void* addr; size_t sz; };

inline static void munmap_(struct mmap_args* args) { if (args && args->addr && args->sz) { munmap(args->addr, args->sz); } }
inline static void close_(int* filedp) { if (filedp && *filedp >= 0) { close(*filedp); } }
#else
inline static void free_(void** addrp) { if (addrp && *addrp) { free(*addrp); } }
#endif
inline static void fclose_(FILE** filedp) { if (filedp && *filedp) { fclose(*filedp); } }
inline static void closedir_(DIR** dirdp) { if (dirdp && *dirdp) { closedir(*dirdp); } }
inline static void free_dtorable_(struct uxnb_dtorable** o) { if (o && *o) { uxnb_free_dtorable(*o); } }
inline static void free_file_(struct uxnb_xnb_file** f) { if (f && *f) { uxnb_free_file(*f); } }

typedef void (*dtor_f)(void);
__attribute__((__unused__)) inline static void cleanupv(dtor_f* d) { if (d && *d) { (*d)(); } }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-macros"
#define MERGE(x, y) x##y
#define MERGE_N(x) MERGE(_DEFER_VAR, x)

#define defer(fn, x) __attribute__((__unused__,__cleanup__(fn))) __typeof(x) MERGE_N(__COUNTER__) = (x)
#define deferv(fn) __attribute__((__unused__,__cleanup__(cleanupv))) dtor_f MERGE_N(__COUNTER__) = (fn)
#pragma clang diagnostic pop

extern bool quiet_flag;
bool quiet_flag;

static void print_version(const char* prog) {
    eprintf("%s version 0.0.0.0\n" COPYRIGHT_MSG, prog);
}
static void print_usage(const char* prog) {
    eprintf(
"%s - unpack XNB and XWB files.\n"
COPYRIGHT_MSG
"Possible invocations:\n"
"%s\t[-h|--help]\n"
"\tDisplay this help text.\n"
"%s\t[-V|--version]\n"
"\tDisplay the program's version.\n"
"%s\t[-q] <infile> [<outpath> [<type>]]\n"
"\tUnpack the input file to the specified output path (directory for\n"
"\twavebanks, file for anything else, defaults to either the current dir\n"
"\tor the input file with a different extension.) Optionally, the type\n"
"\tof content can be specified as well, if it isn't detected automatically.\n"
"\tThe -q flag can be supplied to suppress informational messages.\n",
        prog, prog, prog, prog);
}

static const char* typs[] = {
    "tex"     , UXNB_TEXTURE2D_RTYPE,
    "font"    , UXNB_SPRITEFONT_RTYPE,
    "spritef" , UXNB_SPRITEFONT_RTYPE,
    "sound"   , UXNB_SOUNDEFFECT_RTYPE,
    "waveb"   , UXNB_WB_RTYPE,
    "bank"    , UXNB_WB_RTYPE,
    "eff"     , UXNB_EFFECT_RTYPE,
    "shade"   , UXNB_EFFECT_RTYPE,
  //"dsf"     , UXNB_RLDSF_RTYPE,
  //"dspritef", UXNB_RLDSF_RTYPE,
  //"dsfont"  , UXNB_RLDSF_RTYPE,

    NULL, NULL
};
static const char* stupidPrefixes[] = {
    "Microsoft.Xna.Framework.Content.ContentReaders.",
    "Microsoft.Xna.Framework.Content.",
    "Microsoft.Xna.Framework.Audio.",
    "Microsoft.Xna.Framework.Graphics.",
    "Microsoft.Xna.Framework.",
    "MonoGame.Framework.Content.ContentReaders.",
    "MonoGame.Framework.Content.",
    "MonoGame.Framework.Audio.",
    "MonoGame.Framework.Graphics.",
    "MonoGame.Framework.",
  //"ReLogic.Graphics.",
    NULL
};

static const char* normalise_type(const char* s) {
    for (const char** a = stupidPrefixes; *a; ++a) {
        size_t l = strlen(*a);
        if (!strncasecmp(s, *a, l)) {
            a += l;
            break;
        }
    }

    for (const char** a = typs; *a; a += 2) {
        if (!strncasecmp(s, a[0], strlen(a[0]))) {
            return a[1];
        }
    }

    return NULL;
}

static int write_xwb(const char* oup, struct uxnb_wavebank* xwb) {
    if (!oup) {
        oup = ".";
    }

#ifndef _WIN32
    int dird = open(oup, O_RDONLY|O_DIRECTORY);
    if (dird < 0) {
        eprintf("Could not open output directory '%s'\n", oup);
        return 1;
    }
    defer(close_, dird);
#endif

    for (size_t i = 0; i < xwb->nentries; ++i) {
        // file format for both codecs is WAV, so it doesn't really
        // matter which union member I use
        if (xwb->entries[i].audiofile.pcm) {
            char filen[0x10];
            snprintf(filen, 0xF, "cue_%.3lu.%s", i,
                    (xwb->entries[i].meta.codec == uxnbwbc_wma) ? "wma" : "wav");
            filen[0xF] = '\0';

#ifndef _WIN32
            int filed = openat(dird, filen, O_RDWR|O_CREAT, 0644);
            if (filed < 0) {
                eprintf("Warning: couldn't open file '%s', continuing...\n", filen);
                continue;
            }
            defer(close_, filed);

            ssize_t r = write(filed, xwb->entries[i].audiofile.pcm,
                                     xwb->entries[i].filesz);
#else
            char pathn[strlen(oup) + 2 + 0x10];
            snprintf(pathn, sizeof(pathn)-1, "%s/%s", oup, filen);
            pathn[sizeof(pathn)-1] = '\0';

            FILE* filed = fopen(pathn, "wb");
            if (!filed) {
                eprintf("Warning: couldn't open file '%s', continuing...\n", filen);
                continue;
            }
            defer(fclose_, filed);

            ssize_t r = fwrite(xwb->entries[i].audiofile.pcm, 1,
                               xwb->entries[i].filesz, filed);
#endif

            if (r < 0 || (size_t)r != xwb->entries[i].filesz) {
                eprintf("Warning: couldn't write to file '%s', continuing...\n", filen);
            }
        }
        else {
            eprintf("Warning: cue %lu couldn't be read, continuing...\n", i);
        }
    }

    return 0;
}

inline static enum xnb_type xnb_get_type(struct uxnb_xnb_file* xnb) {
    if (!strncmp(xnb->obj.rtype, UXNB_TEXTURE2D_RTYPE, UXNB_LENOF(UXNB_TEXTURE2D_RTYPE)-1)) {
        return xnbt_tex;
    }
    if (!strncmp(xnb->obj.rtype, UXNB_SPRITEFONT_RTYPE, UXNB_LENOF(UXNB_SPRITEFONT_RTYPE)-1)) {
        return xnbt_fnt;
    }
    if (!strncmp(xnb->obj.rtype, UXNB_SOUNDEFFECT_RTYPE, UXNB_LENOF(UXNB_SOUNDEFFECT_RTYPE)-1)) {
        return xnbt_snd;
    }
    if (!strncmp(xnb->obj.rtype, UXNB_WB_RTYPE, UXNB_LENOF(UXNB_WB_RTYPE)-1)) {
        return xnbt_wb ;
    }
    if (!strncmp(xnb->obj.rtype, UXNB_EFFECT_RTYPE, UXNB_LENOF(UXNB_EFFECT_RTYPE)-1)) {
        return xnbt_sh ;
    }
    /*if (!strncmp(xnb->obj.rtype, UXNB_RLDSF_RTYPE, UXNB_LENOF(UXNB_RLDSF_RTYPE)-1)) {
        return xnbt_dsf;
    }*/

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wassign-enum"
    return ~(enum xnb_type)0;
#pragma clang diagnostic pop
}
static int write_xnb(const char* inf, const char* ouf, struct uxnb_xnb_file* xnb) {
    if (!xnb->obj.data) {
        eprintf("Couldn't read file, data is null.\n");
        return 1;
    }

    enum xnb_type t = xnb_get_type(xnb);

    if (!~t) {
        eprintf("Unknown XNB type %s.\n", xnb->obj.rtype);
        return 1;
    }

    if (t == xnbt_wb) {
        return write_xwb(ouf, (struct uxnb_wavebank*)xnb->obj.data);
    }

    if (t == xnbt_fnt) {
        struct uxnb_spritefont* sf = (struct uxnb_spritefont*)xnb->obj.data;

        if (!sf->texture) {
            eprintf("Couldn't read file, texture data is null.\n");
            return 1;
        }
    }

    size_t infl = strlen(inf);
    char wout[infl + 0x8];
    if (!ouf || !strcmp(ouf, ".")) {
        const char* ext = NULL;
        switch (t) {
            case xnbt_tex:
            case xnbt_fnt: ext = "png"; break;
            case xnbt_snd: ext = "wav"; break;
            case xnbt_sh : ext = "cso"; break;
        }

        size_t lasti = infl;

        for (size_t i = infl; i; --i) {
            if (inf[i - 1] == '.') {
                lasti = i - 1;
                break;
            }
        }

        char fmt[0x1C];
        snprintf(fmt, 0x1B, "%%.%lus.%%s", lasti);
        fmt[0x1B] = '\0';
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
        snprintf(wout, infl + 0x7, fmt, inf, ext);
        wout[infl + 0x7] = '\0';
#pragma clang diagnostic pop

        ouf = wout;
    }

    FILE* filed = fopen(ouf, "wb");
    if (!filed) {
        eprintf("Couldn't open file '%s'.\n", ouf);
        return 1;
    }
    defer(fclose_, filed);

    void* d = NULL;
    size_t sz = xnb->obj.size;
    switch (t) {
        case xnbt_tex:
            {
                struct uxnb_texture2d* tt = (struct uxnb_texture2d*)xnb->obj.data;
                d  = tt->levels[0].png;
                sz = tt->levels[0].filesz;
            }
            break;
        case xnbt_fnt:
            {
                struct uxnb_spritefont* s = (struct uxnb_spritefont*)xnb->obj.data;
                d  = s->texture->levels[0].png;
                sz = s->texture->levels[0].filesz;
            }
            break;
        case xnbt_snd:
            d = &((struct uxnb_soundeffect*)xnb->obj.data)->wavfile;
            break;
        case xnbt_sh:
            d = &((struct uxnb_effect*)xnb->obj.data)->dxfile[0];
            break;
    }

    if (!d) {
        eprintf("Could not read data, actual data is null.\n");
        return 1;
    }

    ssize_t r = fwrite(d, 1, sz, filed);
    if (r < 0 || (size_t)r != sz) {
        eprintf("Couldn't write %zX bytes to file '%s' from address %p.\n", sz, ouf, d);
        return 1;
    }

    //// TODO: output metadata for spritefonts
    // TODO: when that works, that is...

    return 0;
}

static int unpack(const char* infile, const char* outpath, const char* typehint) {
#ifndef _WIN32
    int filed = open(infile , O_RDONLY);
    if (filed < 0) {
        eprintf("Could not open input file '%s'\n", infile);
        return 1;
    }
    defer(close_, filed);

    struct stat st;
    fstat(filed, &st);
    size_t length = (size_t)st.st_size;

    void* addr = mmap(NULL, (size_t)st.st_size, PROT_READ, MAP_SHARED, filed, 0);
    if (!addr || addr == MAP_FAILED) {
        eprintf("Could not mmap the input file.\n");
        return 1;
    }
    defer(munmap_, ((struct mmap_args){.addr=addr,.sz=(size_t)st.st_size}));
#else
    FILE* filed = fopen(infile, "rb");
    if (!filed) {
        eprintf("Could not open input file '%s'\n", infile);
        return 1;
    }
    defer(fclose_, filed);

    if (fseek(filed, 0, SEEK_END)) {
        eprintf("Cannot seek input file '%s'\n", infile);
        return 1;
    }
    long length = ftell(filed);
    fseek(filed, 0, SEEK_SET);

    void* addr = malloc(length);
    if (!addr) {
        eprintf("Could not allocate enough memory to load the input file.\n");
        return 1;
    }
    defer(free_, addr);
#endif

    uint32_t h = *(uint32_t*)addr;
    if (h == to_magic(UXNB_WB_MAGIC)) {
        struct uxnb_wavebank* r  = uxnb_read_wavebank(addr, NULL, NULL);
        if (!r) {
            eprintf("Could not read wavebank file.\n");
            return 1;
        }
        defer(free_dtorable_, (struct uxnb_dtorable*)r);

        return write_xwb(outpath, r);
    }

    h &= 0x00FFFFFF; // ignore platform byte
    if (h == to_magic3(UXNB_XNB_MAGIC)) {
        struct uxnb_xnb_file* r = uxnb_read_xnb(addr, typehint, length);
        if (!r) {
            eprintf("Could not read XNB file.\n");
            return 1;
        }
        defer(free_file_, r);

        return write_xnb(infile, outpath, r);
    }

    eprintf("Unknown file magic '%.4s'.\n", (const char*)addr);
    return 1;
}

int main(int argc, char* argv[]) {
    if (argc == 2) {
        uint32_t h = str_hash_djb2(argv[1]);

        if (h == str_hash_djb2("--help") || h == str_hash_djb2("-h")) {
            print_usage(argv[0]);
            return 0;
        }
        else if (h == str_hash_djb2("--version") || h == str_hash_djb2("-V")) {
            print_version(argv[0]);
            return 0;
        }
    }

    if (argc < 2) {
        print_usage(argv[0]);
        return 1;
    }

    int off = 0;
    if (!strcmp(argv[1], "-q")) {
        ++off;
        --argc;
        quiet_flag = true;
    }

    const char* inf = argv[1 + off];
    const char* oup = NULL;
    const char* rth = NULL;

    if (argc > 2) {
        oup = argv[2 + off];

        if (argc > 3) {
            rth = normalise_type(argv[3 + off]);
        }
    }

    return unpack(inf, oup, rth);
}

