
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "xnb.h"

#include "effect.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
struct uxnb_effect* uxnb_read_effect(void* data, size_t* sz, struct uxnb_xnb_fmt* fmt) {
#pragma clang diagnostic pop
    uint32_t size = *(uint32_t*)data;

    if (sz) {
        *sz = size;
    }

    struct uxnb_effect* r = (struct uxnb_effect*)
        malloc(size + sizeof(uxnb_dtor_f));

    r->free_me = NULL;
    memcpy(&r->dxfile[0], (uint8_t*)data + sizeof(uint32_t), size);

    return r;
}

