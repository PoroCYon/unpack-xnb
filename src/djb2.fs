module DJB2

let djb2 (s: string) =
    let mutable r = 5381u
    for c in s do
        r <- (r <<< 5) + r + uint32 c
    r

