
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "dxt.h"

struct rgb {
    uint16_t b : 5;
    uint16_t g : 6;
    uint16_t r : 5;
};
struct rgba {
    uint8_t a, r, g, b;
};

struct dxt1b {
    struct rgb base[2];
    uint32_t lut;
};
struct dxt3b {
    uint8_t a[8];
    struct rgb base[2];
    uint32_t lut;
};
struct dxt5b {
    uint8_t a[2];
    uint8_t mask[6];

    struct rgb base[2];
    uint32_t lut;
};

inline static struct rgba rgb565to888(struct rgb v) {
    struct rgba r;
    size_t

    t = ((size_t)v.r * 0xFF) + 0x10;
    r.r = (uint8_t)(((t >> 5) + t) >> 5);
    t = ((size_t)v.g * 0xFF) + 0x20;
    r.g = (uint8_t)(((t >> 6) + t) >> 6);
    t = ((size_t)v.b * 0xFF) + 0x10;
    r.b = (uint8_t)(((t >> 5) + t) >> 5);

    r.a ^= ~r.a; // a = 0xFF

    return r;
}

static void undxt1_block(struct rgba* dest, struct dxt1b src,
        size_t x, size_t y, size_t w, size_t h) {
    struct rgba c0 = rgb565to888(src.base[0]),
                c1 = rgb565to888(src.base[1]);
    uint32_t r0 = c0.r, r1 = c1.r,
             g0 = c0.g, g1 = c1.g,
             b0 = c0.b, b1 = c1.b;

    for (size_t yy = 0; !(yy & 0x4); ++yy) {
        for (size_t xx = 0; !(xx & 0x4); ++xx) {
            struct rgba c;
            c.a = c0.a;

            size_t ind = (src.lut >> (((yy << 2) + xx) << 1)) & 0x03;

            if (*(uint16_t*)&src.base[0] > *(uint16_t*)&src.base[1]) {
                switch (ind) {
                    case 0:
                        c = c0;
                        break;
                    case 1:
                        c = c1;
                        break;
                    case 2:
                        c.r = (uint8_t)(((r0 << 1) + r1) / 3);
                        c.g = (uint8_t)(((g0 << 1) + g1) / 3);
                        c.b = (uint8_t)(((b0 << 1) + b1) / 3);
                        break;
                    case 3:
                        c.r = (uint8_t)((r0 + (r1 << 1)) / 3);
                        c.g = (uint8_t)((g0 + (g1 << 1)) / 3);
                        c.b = (uint8_t)((b0 + (b1 << 1)) / 3);
                        break;
                }
            }
            else {
                switch (ind) {
                    case 0:
                        c = c0;
                        break;
                    case 1:
                        c = c1;
                        break;
                    case 2:
                        c.r = (uint8_t)((r0 + r1) >> 1);
                        c.g = (uint8_t)((g0 + g1) >> 1);
                        c.b = (uint8_t)((b0 + b1) >> 1);
                        break;
                    case 3:
                        c.a = c.b = c.g = c.r ^= c.r;
                        break;
                }
            }


            size_t p = (x << 2) | xx,
                   q = (y << 2) | yy;
            if (p < w && q < h) {
                dest[(q * w) + p] = c;
            }
        }
    }
}
static void undxt3_block(struct rgba* dest, struct dxt3b* src,
        size_t x, size_t y, size_t w, size_t h) {
    struct rgba c0 = rgb565to888(src->base[0]),
                c1 = rgb565to888(src->base[1]);
    uint32_t r0 = c0.r, r1 = c1.r,
             g0 = c0.g, g1 = c1.g,
             b0 = c0.b, b1 = c1.b;

    for (size_t aind = 0, yy = aind; !(yy & 0x4); ++yy) {
        for (size_t xx = 0; !(xx & 0x4); ++xx, ++aind) {
            struct rgba c;

            size_t aa = src->a[aind >> 1];
            aa  = aa & (0xFuL << ((aind & 0x1) << 3));
            aa |= aa << 4;
            c.a = (uint8_t)aa;

            size_t ind = (src->lut >> (((yy << 2) + xx) << 1)) & 0x03;
            switch (ind) {
                case 0:
                    c = c0;
                    break;
                case 1:
                    c = c1;
                    break;
                case 2:
                    c.r = (uint8_t)(((r0 << 1) + r1) / 3);
                    c.g = (uint8_t)(((g0 << 1) + g1) / 3);
                    c.b = (uint8_t)(((b0 << 1) + b1) / 3);
                    break;
                case 3:
                    c.r = (uint8_t)((r0 + (r1 << 1)) / 3);
                    c.g = (uint8_t)((g0 + (g1 << 1)) / 3);
                    c.b = (uint8_t)((b0 + (b1 << 1)) / 3);
                    break;
            }

            size_t p = (x << 2) | xx,
                   q = (y << 2) | yy;
            if (p < w && q < h) {
                dest[(q * w) + p] = c;
            }
        }
    }
}
static void undxt5_block(struct rgba* dest, struct dxt5b* src,
        size_t x, size_t y, size_t w, size_t h) {
    struct rgba c0 = rgb565to888(src->base[0]),
                c1 = rgb565to888(src->base[1]);
    uint32_t r0 = c0.r, r1 = c1.r,
             g0 = c0.g, g1 = c1.g,
             b0 = c0.b, b1 = c1.b;

    uint64_t amask = src->mask[0];
    for (size_t i = 1; i < 6; ++i) {
        amask |= (uint64_t)src->mask[i] << (i << 3);
    }

    for (size_t yy = 0; !(yy & 0x4); ++yy) {
        for (size_t xx = 0; !(xx & 0x4); ++xx) {
            struct rgba c;

            size_t aind = (size_t)((amask >> (3 * ((yy << 2) + xx))) & 0x07);
            switch (aind) {
                case 0:
                    c.a = src->a[0];
                    break;
                case 1:
                    c.a = src->a[1];
                    break;
                case 6:
                    c.a ^= c.a;
                    break;
                case 7:
                    c.a ^= ~c.a;
                    break;
                default:
                    c.a = (uint8_t)((((src->a[0] > src->a[1] ? 8 : 6) - aind) * src->a[0]
                                    + (aind - 1) * src->a[1])
                            / (src->a[0] > src->a[1] ? 7 : 5));
                    break;
            }

            size_t ind = (src->lut >> (((yy << 2) + xx) << 1)) & 0x03;
            switch (ind) {
                case 0:
                    c = c0;
                    break;
                case 1:
                    c = c1;
                    break;
                case 2:
                    c.r = (uint8_t)(((r0 << 1) + r1) / 3);
                    c.g = (uint8_t)(((g0 << 1) + g1) / 3);
                    c.b = (uint8_t)(((b0 << 1) + b1) / 3);
                    break;
                case 3:
                    c.r = (uint8_t)((r0 + (r1 << 1)) / 3);
                    c.g = (uint8_t)((g0 + (g1 << 1)) / 3);
                    c.b = (uint8_t)((b0 + (b1 << 1)) / 3);
                    break;
            }

            size_t p = (x << 2) | xx,
                   q = (y << 2) | yy;
            if (p < w && q < h) {
                dest[(q * w) + p] = c;
            }
        }
    }
}

void uxnb_unpack_dxt1(uint32_t* dest, void* src, size_t w, size_t h) {
    struct dxt1b* sr = (struct dxt1b*)src;

    size_t bx = (w + 3) >> 2,
           by = (h + 3) >> 2;

    for (size_t i = 0, y = i; y < by; ++y) {
        for (size_t x = 0; x < bx; ++x, ++i) {
            undxt1_block((struct rgba*)dest, sr[i], x, y, w, h);
        }
    }
}
void uxnb_unpack_dxt3(uint32_t* dest, void* src, size_t w, size_t h) {
    struct dxt3b* sr = (struct dxt3b*)src;

    size_t bx = (w + 3) >> 2,
           by = (h + 3) >> 2;

    for (size_t i = 0, y = i; y < by; ++y) {
        for (size_t x = 0; x < bx; ++x, ++i) {
            undxt3_block((struct rgba*)dest, &sr[i], x, y, w, h);
        }
    }
}
void uxnb_unpack_dxt5(uint32_t* dest, void* src, size_t w, size_t h) {
    struct dxt5b* sr = (struct dxt5b*)src;

    size_t bx = (w + 3) >> 2,
           by = (h + 3) >> 2;

    for (size_t i = 0, y = i; y < by; ++y) {
        for (size_t x = 0; x < bx; ++x, ++i) {
            undxt5_block((struct rgba*)dest, &sr[i], x, y, w, h);
        }
    }
}

