
#ifndef UXNB_UTIL_H_
#define UXNB_UTIL_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "xnb.h"

#define UXNB_LENOF(arr) (sizeof(arr)/sizeof(arr[0]))

struct lpstr {
    size_t length;
    const char* data;
};

inline static void eprintf_backend(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    vfprintf(stderr, fmt, args);
#pragma clang diagnostic pop
    va_end(args);
}
#ifndef NDEBUG
#define eprintf(fmt, ...) eprintf_backend("[%s:%s:%i]\t" fmt, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define eprintf(fmt, ...) eprintf_backend(fmt, ##__VA_ARGS__)
#endif

inline static size_t readvlq(uint8_t** pp) {
    uint8_t* p = *pp;

    size_t r = 0;
    for (size_t i = 0; !(i & 0x8); ++p, ++i) {
        r |= (size_t)(*p & 0x7F) << (i * 7);

        if (!(*p & 0x80)) {
            break;
        }
    }

    *pp = p + 1;

    return r;
}

__attribute__((__pure__))
inline static uint32_t str_hash_djb2(const char* s) {
    uint32_t r = 5381;

    for (char c; (c = *s); ++s) {
        r += (r << 5) + (uint32_t)c;
    }

    return r;
}
__attribute__((__pure__))
inline static uint32_t strn_hash_djb2(const char* s, size_t maxlen) {
    uint32_t r = 5381;

    size_t i = 0;
    for (char c; (c = *s) && i < maxlen; ++s, ++i) {
        r += (r << 5) + (uint32_t)c;
    }

    return r;
}

inline static char* mksz(struct lpstr lp) {
    if (!lp.length || !lp.data) {
        return NULL;
    }

    char* s = (char*)calloc(sizeof(char), lp.length + 1);

    memcpy(s, lp.data, lp.length);
    s[lp.length] = 0;

    return s;
}

inline static struct lpstr readlps(uint8_t** pp) {
    size_t l = readvlq(pp);

    struct lpstr r;

    r.data = (char*)*pp;
    *pp = *pp
        + (r.length = l);

    return r;
}

__attribute__((__pure__, __always_inline__))
inline static uint32_t to_magic(const char s[4]) {
    // assuming little-endian
    return  (uint32_t)s[0]        | ((uint32_t)s[1] <<  8)
         | ((uint32_t)s[2] << 16) | ((uint32_t)s[3] << 24);
}
__attribute__((__pure__, __always_inline__))
inline static uint32_t to_magic3(const char s[3]) {
    // assuming little-endian
    return  (uint32_t)s[0]        | ((uint32_t)s[1] <<  8)
         | ((uint32_t)s[2] << 16);
}

#endif

