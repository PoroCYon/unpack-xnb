
#ifndef UXNB_XNB_H_
#define UXNB_XNB_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

//#define TOMAGIC_(b,str) ((b)?((*(str))|(TOMAGIC_(b-1,str)<<8)):0)
//#define TOMAGIC(str) TOMAGIC_(sizeof(str)/sizeof(*(str))-1,str)

#define UXNB_MIN_VERSION (4)
#define UXNB_MAX_VERSION (5)
#define UXNB_XNB_MAGIC ("XNB")

enum uxnb_platform {
    uxnbp_windows = 'w',
    uxnbp_xbox    = 'x',
    uxnbp_wphone  = 'm',
    uxnbp_ithings = 'i',
    uxnbp_android = 'a',
    uxnbp_opengl  = 'd',
    uxnbp_macosx  = 'X',
    uxnbp_wstore  = 'W',
    uxnbp_nclient = 'n',
    uxnbp_ouya    = 'u',
    uxnbp_psmobil = 'p',
    uxnbp_wphone8 = 'M',
    uxnbp_raspbpi = 'r',
    uxnbp_ps4     = 'P',
    uxnbp_win32gl = 'g', // deprecated: use uxnbp_opengl instead
    uxnbp_linuxgl = 'l'  // deprecated: use uxnbp_opengl instead
};

enum uxnb_flags {
    uxnbf_hidef          = 0x01, // ignored
    uxnbf_compressed_lz4 = 0x40, // FIXME: unsupported
    uxnbf_compressed_lzx = 0x80
};

struct uxnb_dtorable;
typedef void (*uxnb_dtor_f)(struct uxnb_dtorable*);
struct uxnb_dtorable {
    uxnb_dtor_f free_me;
    uint8_t data[];
};

struct uxnb_object {
    const char* rtype;
    void* rawdata;
    size_t size;
    struct uxnb_dtorable* data;
};

#pragma pack(push,1)
struct uxnb_xnb_fmt {
    char magic[3];
    uint8_t platform;
    uint8_t xnaver;
    uint8_t flags;
    uint32_t filesz;
    // size_t nreaders
    // struct uxnb_typereader[]
    // size_t nsharedresources
    uint8_t _data[];
};
#pragma pack(pop)

struct uxnb_xnb_file {
    struct uxnb_xnb_fmt* compr;

    uint16_t platform;
    uint16_t xnaver;
    enum uxnb_flags flags;

    struct uxnb_object obj;

    size_t nsharedresources;
    struct uxnb_object sharedresources[];
};

void uxnb_free_dtorable(struct uxnb_dtorable* o);

struct uxnb_xnb_file* uxnb_read_xnb(void* data,
        const char* typehint, size_t filesz);

void uxnb_free_file(struct uxnb_xnb_file* file);

#endif

