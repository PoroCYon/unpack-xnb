
#ifndef UXNB_WAVEBANK_H_
#define UXNB_WAVEBANK_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "wave.h"
#include "xnb.h"
#include "soundeffect.h"

#define UXNB_WB_MAGIC ("WBND")
#define UXNB_WB_AEVERSION (46)
#define UXNB_WB_WBVERSION (44)
#define UXNB_WB_RTYPE ("Microsoft.Xna.Framework.Audio.WaveBank")

enum uxnb_wavebank_codec {
    uxnbwbc_pcm   = 0x0,
    uxnbwbc_xma   = 0x1,
    uxnbwbc_adpcm = 0x2,
    uxnbwbc_wma   = 0x3
};

struct uxnb_waveentry_meta {
    uint32_t codec      :  2;
    uint32_t channels   :  3;
    uint32_t samplerate : 18;
    uint32_t alignment  :  8;
    uint32_t bitdepth   :  1;
};

struct uxnb_waveentry {
    size_t filesz;
    struct uxnb_waveentry_meta meta;

    union {
        struct uxnb_wave_pcm  *   pcm;
        // TODO: xma
        struct uxnb_wave_adpcm* adpcm;
        struct uxnb_wave_xwma *  xwma;
    } audiofile;
};
struct uxnb_wavebank {
    uxnb_dtor_f free_me;
    size_t nentries;
    struct uxnb_waveentry entries[];
};

struct uxnb_wavebank* uxnb_read_wavebank(void* data, size_t* sz, struct uxnb_xnb_fmt* fmt);

#endif

