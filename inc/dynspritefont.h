
#ifndef UXNB_DYNSPRITEFONT_H_
#define UXNB_DYNSPRITEFONT_H_

// TODO: implement

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "xnb.h"
#include "texture2d.h"
#include "spritefont.h"

#define UXNB_RLDSF_RTYPE ("ReLogic.Graphics.DynamicSpriteFontReader")

struct uxnb_dsf_page {
    struct uxnb_texture2d* texture;

    size_t nglyphs;
    size_t npadding;
    size_t nchars;
    size_t nkerns;

    struct uxnb_rect* glyphs;
    struct uxnb_rect* padding;
    char* charmap;
    struct uxnb_vec3* kerning;
};

struct uxnb_dynspritefont {
    float spacing;
    int32_t line_spacing;
    char default_char;

    size_t npages;
    struct uxnb_dsf_page pages[];
};

struct uxnb_dynspritefont* uxnb_read_dsf(void* data, size_t* sz,
        struct uxnb_xnb_fmt* fmt);

#endif

