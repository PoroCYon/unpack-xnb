
#ifndef UXNB_SPRITEFONT_H_
#define UXNB_SPRITEFONT_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "xnb.h"
#include "texture2d.h"

#define UXNB_SPRITEFONT_RTYPE ("Microsoft.Xna.Framework.Content.SpriteFontReader")

struct uxnb_rect {
    int32_t x, y, w, h;
};
struct uxnb_vec3 {
    float x, y, z;
};

struct uxnb_spritefont {
    uxnb_dtor_f free_me;
    struct uxnb_texture2d* texture;

    size_t nglyphs;
    size_t ncrops ;
    size_t nchars ;
    size_t nkerns ;

    struct uxnb_rect* glyphs;
    struct uxnb_rect* crops ;
    char* charmap;
    struct uxnb_vec3* kerning;

    int32_t linespacing;
    float spacing;
    char default_char;
};

struct uxnb_spritefont* uxnb_read_spritefont(void* data, size_t* sz, struct uxnb_xnb_fmt* fmt);

#endif

