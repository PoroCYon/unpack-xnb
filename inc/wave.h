
#ifndef UXNB_WAVE_H_
#define UXNB_WAVE_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "util.h"

#define UXNB_ADPCM_BITDEPTH ( 4)
#define UXNB_ADPCM_NCOEFFS  ( 7)
#define UXNB_XWMA_AVGBPSES  ( 6)
#define UXNB_XWMA_BLKALIGNS (16)

#define UXNB_MAGIC_RIFF ("RIFF")
#define UXNB_MAGIC_WAVE ("WAVE")
#define UXNB_MAGIC_XWMA ("XWMA")
#define UXNB_MAGIC_fmt_ ("fmt ")
#define UXNB_MAGIC_dpds ("dpds")
#define UXNB_MAGIC_data ("data")
#define UXNB_MAGIC_fact ("fact")

// WAV is batshit insane
enum uxnb_wave_fmt {
    uxnbwf_unk   = 0x0000,
    uxnbwf_pcm   = 0x0001,
    uxnbwf_ms_ad = 0x0002, // ADPCM (MS)
    uxnbwf_float = 0x0003,
    uxnbwf_cvsd  = 0x0005, // IBM CVSD
    uxnbwf_alaw  = 0x0006, // A-law
    uxnbwf_mulaw = 0x0007, // µ-law
    uxnbwf_okiad = 0x0010, // ADPCM (OKI)
    uxnbwf_imaad = 0x0011, // ADPCM (Intel)
    uxnbwf_mspad = 0x0012, // ADPCM (Mediaspace)
    uxnbwf_sraad = 0x0013, // ADPCM (Sierra)
    uxnbwf_g23ad = 0x0014, // ADPCM (G.723)
    uxnbwf_distd = 0x0015, // ?
    uxnbwf_iduad = 0x0016, // ADPCM (ITU, G.723)
    uxnbwf_okida = 0x0017, // ADPCM (Dialogic OKI)
    uxnbwf_ymhad = 0x0020, // ADPCM (Yamaha)
    uxnbwf_sonrc = 0x0021, // Sonarc
    uxnbwf_trusp = 0x0022, // TrueSpeech (DSP Group)
    uxnbwf_ecsc1 = 0x0023, // Echo SC 1
    uxnbwf_af36  = 0x0024, // AF36 (AudioFile)
    uxnbwf_aptx  = 0x0025, // APTX
    uxnbwf_af10  = 0x0026, // AF10 (AudioFile)
    uxnbwf_dbaac = 0x0030, // AAC (Dolby)
    uxnbwf_msgsm = 0x0031, // GSM (MS)
    uxnbwf_atxad = 0x0033, // ADPCME (Antex)
    uxnbwf_vqlpc = 0x0034, // VQLPC (Control Resources)
    uxnbwf_dgrl  = 0x0035, // DigiReal
    uxnbwf_rwadp = 0x0036, // ADPCM (Rockwell)
    uxnbwf_cr10  = 0x0037, // CR10 (Control Resources)
    uxnbwf_vbxad = 0x0038, // VBXADPCM (Natural MicroSystems)
    uxnbwf_rwad_ = 0x003B, // ADPCM (Rockwell, again?)
    uxnbwf_rwdgt = 0x003C, // DigiTalk (Rockwell)
    uxnbwf_itu1a = 0x0040, // ADPCM (ITU, G.721)
    uxnbwf_celp8 = 0x0041, // G.728 (CELP)
    uxnbwf_ms723 = 0x0042, // G.723 (MS)
    uxnbwf_itut6 = 0x0045, // G.726 (ITU-T)
    uxnbwf_mpeg  = 0x0050, // MPEG
    uxnbwf_mpeg3 = 0x0055, // MPEG-3
    uxnbwf_apiad = 0x0064, // ADPCM (APICOM, G.726)
    uxnbwf_apia2 = 0x0065, // ADPCM (APICOM, G.722)
    uxnbwf_xma2  = 0x0069, // XMA 2 (Microsoft)
    uxnbwf_xwma  = 0x0161, // xWMA (MICROSOFT) // NOTE: XWMA, not WAVE!
    uxnbwf_ibmmu = 0x0101, // A-law (IBM)
    uxnbwf_ibma  = 0x0102, // µ-law (IBM)
    uxnbwf_ibmad = 0x0103, // ADPCM (IBM)
    uxnbwf_crtad = 0x0200, // ADPCM (Creative)
    uxnbwf_towns = 0x0300, // FM TOWNS SND (Fujitsu)
    uxnbwf_olgsm = 0x1000, // GSM (Olivetti)
    uxnbwf_oliad = 0x1001, // ADPCM (Olivetti)
    uxnbwf_ocelp = 0x1002, // CELP (Olivetti)
    uxnbwf_olsbc = 0x1003, // SBC (Olivetti)
    uxnbwf_olopr = 0x1004, // OPR (Olivetti)
    uxnbwf_ext   = 0xFFFE
};

// TODO: give these structs correct names
#pragma pack(push, 1)
struct uxnb_riff {
    uint32_t magic;
    uint32_t size;
};
struct uxnb_wave_header {
    struct uxnb_riff riff;
    uint32_t wave_magic;
    struct uxnb_riff fmt;
};
struct uxnb_wave_shared {
    uint16_t format;
    uint16_t channels;
    uint32_t samplerate;
    uint32_t byterate;
    uint16_t blockalign;
    uint16_t bitdepth;
};

struct uxnb_wavefmt_pcm {
    struct uxnb_wave_header header;
    struct uxnb_wave_shared shared;
    struct uxnb_riff data;
};
struct uxnb_wave_pcm {
    struct uxnb_wavefmt_pcm fmt;
    uint8_t samples[];
};

struct uxnb_adpcm_cb {
    uint16_t samples_block;
    uint16_t ncoeffs; // constant 7, because fuck non-PCM WAV
    uint16_t coeffs[14]; // 2 * ncoeffs
};
struct uxnb_wavefmt_adpcm {
    struct uxnb_wave_header header;
    struct uxnb_wave_shared shared;
    uint16_t cbsize; // constant sizeof(uxnb_adpcm_cb)
    struct uxnb_adpcm_cb cb;
    struct uxnb_riff fact; // size: constant 4
    uint32_t fact_data;
    struct uxnb_riff data;
};
struct uxnb_wavefmt_adpcm_ {
    struct uxnb_wave_shared shared;
    uint16_t cbsize; // constant sizeof(uxnb_adpcm_cb)
    struct uxnb_adpcm_cb cb;
};
struct uxnb_wave_adpcm {
    struct uxnb_wavefmt_adpcm fmt;
    uint8_t samples[];
};

struct uxnb_wavefmt_xwma {
    struct uxnb_wave_header header;
    struct uxnb_wave_shared shared;
    uint16_t unk0;
    struct uxnb_riff dpds;
    // uint32_t dpds[dpds.size/4];
};
struct uxnb_wavefmt_xwma_ {
    struct uxnb_riff data;
};

struct uxnb_wave_xwma {
    struct uxnb_wavefmt_xwma fmt;
    uint32_t dpds[/* fmt.dpds.size/4 */];
    //struct uxnb_wavefmt_xwma_ datahdr;
    //uint8_t data[];
};
struct uxnb_wave_xwma_ {
    struct uxnb_wavefmt_xwma_ hdr;
    uint8_t data[];
};
#pragma pack(pop)

static const short adpcm_coeffs[UXNB_ADPCM_NCOEFFS << 1] = {
    0x0100,  0x0000,
    0x0200, -0x0100,
    0x0000,  0x0000,
    0x00C0,  0x0040,
    0x00F0,  0x0000,
    0x01CC, -0x00D0,
    0x0188, -0x00E8
};

static const uint32_t uxnb_wma_avgbps[UXNB_XWMA_AVGBPSES] = {
    12000, 24000,  4000,  6000,  8000, 20000
};
static const uint16_t uxnb_wma_blockalign[UXNB_XWMA_BLKALIGNS] = {
     929, 1387, 1280, 2230, 8917, 8192, 4459, 5945,
    2304, 1536, 1485, 1008, 2731, 4096, 6827, 5462
};

inline static void uxnb_wave_fill_pcm_header(struct uxnb_wavefmt_pcm* pcm, size_t pcmsz) {
    pcm->header.riff.magic = to_magic(UXNB_MAGIC_RIFF);
    pcm->header.wave_magic = to_magic(UXNB_MAGIC_WAVE);
    pcm->header.fmt .magic = to_magic(UXNB_MAGIC_fmt_);
    pcm->data.magic        = to_magic(UXNB_MAGIC_data);

    pcm->header.riff.size = (uint32_t)(pcmsz - (sizeof(uint32_t) << 1));
    pcm->header.fmt .size = sizeof(struct uxnb_wave_shared);

    pcm->data.size = (uint32_t)(pcmsz - sizeof(struct uxnb_wavefmt_pcm));

    pcm->shared.format = uxnbwf_pcm;
}
inline static void uxnb_wave_fill_adpcm_header(struct uxnb_wavefmt_adpcm* adpcm, size_t adpcmsz) {
    adpcm->header.riff.magic = to_magic(UXNB_MAGIC_RIFF);
    adpcm->header.wave_magic = to_magic(UXNB_MAGIC_WAVE);
    adpcm->header.fmt .magic = to_magic(UXNB_MAGIC_fmt_);
    adpcm->data.magic        = to_magic(UXNB_MAGIC_data);
    adpcm->fact.magic        = to_magic(UXNB_MAGIC_fact);

    adpcm->header.riff.size = (uint32_t)(adpcmsz - (sizeof(uint32_t) << 1));
    adpcm->header.fmt .size = sizeof(struct uxnb_wavefmt_adpcm_);
    adpcm->cbsize = sizeof(struct uxnb_adpcm_cb);
    adpcm->fact.size = sizeof(uint32_t);

    adpcm->data.size = (uint32_t)(adpcmsz - sizeof(struct uxnb_wavefmt_adpcm));

    adpcm->shared.format = uxnbwf_ms_ad;

    adpcm->shared.bitdepth = UXNB_ADPCM_BITDEPTH;
    adpcm->cb.ncoeffs      = UXNB_ADPCM_NCOEFFS ;
    memcpy(&adpcm->cb.coeffs[0], &adpcm_coeffs[0], sizeof(adpcm_coeffs));
}

inline static void uxnb_wave_fill_xwma_header(struct uxnb_wavefmt_xwma* xwma,
        size_t npacket) {
    xwma->header.riff.magic = to_magic(UXNB_MAGIC_RIFF);
    xwma->header.wave_magic = to_magic(UXNB_MAGIC_XWMA);
    xwma->header.fmt .magic = to_magic(UXNB_MAGIC_fmt_);
    xwma->dpds.magic        = to_magic(UXNB_MAGIC_dpds);

    xwma->header.fmt.size = sizeof(struct uxnb_wave_shared) + sizeof(uint16_t)/*unk0*/;
    xwma->shared.format   = uxnbwf_xwma;
    xwma->shared.bitdepth = 16;

    xwma->dpds.size = (uint32_t)(npacket * sizeof(uint32_t));

    xwma->unk0 = 0;
}
inline static void uxnb_wave_fill_xwma_data(struct uxnb_wavefmt_xwma_* xwma,
        size_t datalen) {
    xwma->data.magic = to_magic(UXNB_MAGIC_data);
    xwma->data.size  = (uint32_t)datalen;
}

#endif

