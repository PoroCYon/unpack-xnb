
#ifndef UXNB_TEXTURE2D_H_
#define UXNB_TEXTURE2D_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "xnb.h"

#define UXNB_TEXTURE2D_RTYPE ("Microsoft.Xna.Framework.Content.Texture2DReader")

struct uxnb_tex2d_level {
    size_t filesz;
    uint32_t width ;
    uint32_t height;

    void* png;
};

struct uxnb_texture2d {
    uxnb_dtor_f free_me;

    size_t nlevels;
    struct uxnb_tex2d_level levels[];
};

struct uxnb_texture2d* uxnb_read_texture2d(void* data, size_t* sz,
        struct uxnb_xnb_fmt* fmt, size_t* bsz);

#endif

