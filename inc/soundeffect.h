
#ifndef UXNB_SOUNDEFFECT_H_
#define UXNB_SOUNDEFFECT_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "wave.h"
#include "xnb.h"

#define UXNB_SOUNDEFFECT_RTYPE ("Microsoft.Xna.Framework.Content.SoundEffectReader")

struct uxnb_soundeffect {
    uxnb_dtor_f free_me;
    size_t wavesz;

    uint32_t loopstart;
    uint32_t looplength;
    uint32_t length_ms;

    struct uxnb_wave_pcm wavfile;
};

struct uxnb_soundeffect* uxnb_read_soundeffect(void* data, size_t* sz, struct uxnb_xnb_fmt* fmt);

#endif

