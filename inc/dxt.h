
#ifndef UXNB_DXT_H_
#define UXNB_DXT_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

void uxnb_unpack_dxt1(uint32_t* dest, void* src, size_t w, size_t h);
void uxnb_unpack_dxt3(uint32_t* dest, void* src, size_t w, size_t h);
void uxnb_unpack_dxt5(uint32_t* dest, void* src, size_t w, size_t h);

#endif

