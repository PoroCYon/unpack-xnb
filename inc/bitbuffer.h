
#ifndef UXNB_BITBUFFER_H_
#define UXNB_BITBUFFER_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

struct uxnb_bitbuffer {
    uint8_t** data;
    uint32_t buffer;
    uint8_t bitsleft;
};

static inline
void uxnb_bitbuffer_init(struct uxnb_bitbuffer* bb, uint8_t** data) {
    bb->data = data;

    bb->buffer   = 0;
    bb->bitsleft = 0;
}

static
void uxnb_bitbuffer_ensure(struct uxnb_bitbuffer* bb, uint8_t bits) {
    while (bb->bitsleft < bits) {
        uint32_t lo = **bb->data; ++(*bb->data);
        uint32_t hi = **bb->data; ++(*bb->data);

        size_t shift_amt = sizeof(uint32_t) * 8 - 16 - bb->bitsleft;

        bb->buffer |= ((hi << 8) | lo) << shift_amt;
        bb->bitsleft += 0x10;
    }
}

static inline
uint32_t uxnb_bitbuffer_peek(struct uxnb_bitbuffer* bb, uint8_t bits) {
    return bb->buffer >> ((sizeof(uint32_t) * 8) - bits);
}

static inline
void uxnb_bitbuffer_remove(struct uxnb_bitbuffer* bb, uint8_t bits) {
    bb->buffer  <<= bits;
    bb->bitsleft -= bits;
}

static inline
uint32_t uxnb_bitbuffer_read(struct uxnb_bitbuffer* bb, uint8_t bits) {
    if (bits) {
        uxnb_bitbuffer_ensure(bb, bits);
        uint32_t r = uxnb_bitbuffer_peek(bb, bits);
        uxnb_bitbuffer_remove(bb, bits);
        return r;
    }

    return 0;
}

#endif

