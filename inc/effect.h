
#ifndef UXNB_EFFECT_H_
#define UXNB_EFFECT_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "xnb.h"

#define UXNB_EFFECT_RTYPE ("Microsoft.Xna.Framework.Content.EffectReader")

struct uxnb_effect {
    uxnb_dtor_f free_me;
    uint8_t dxfile[];
};

struct uxnb_effect* uxnb_read_effect(void* data, size_t* sz, struct uxnb_xnb_fmt* fmt);

#endif

